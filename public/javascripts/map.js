var map = L.map('main_map').setView([9.0515766, -79.4940468], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
/*
L.marker([9.0515766, -79.4940448]).addTo(map);
L.marker([9.0515766, -79.5040418]).addTo(map);
L.marker([9.0515766, -79.4840468]).addTo(map);
*/
$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result) {
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(map);
        });
    }
})