var Bicicleta = require('../models/bicicleta');

/*exports.bicicleta_list = function(req, res) {
    res.render('bicicletas/index', { bicis: Bicicleta.allBicis });
    console.log('bicicletas/index', { bicis: Bicicleta.allBicis });
}*/

exports.bicicleta_list = function(req, res) {
    Bicicleta.allBicis(function(error, result) {
        res.render('bicicletas/index', { bicis: result });
    });
}

exports.bicicleta_create_get = function(req, res) {
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res) {
    /*var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);

    res.redirect('/bicicletas');*/

    var bici = new Bicicleta({
        id: req.body.id,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    });

    Bicicleta.add(bici, function(error, newBici) {
        res.redirect('/bicicletas');
    });
}

exports.bicicleta_update_get = function(req, res) {
    /*var bici = Bicicleta.findById(req.params.id);

    res.render('bicicletas/update', { bici });*/

    Bicicleta.findById(req.params.id, function(err, bici) {
        //console.log(req.params);
        //console.log(bici);
        res.render('bicicletas/update', { bici });
    });
}

exports.bicicleta_update_post = function(req, res) {
    /*var bici = Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    res.redirect('/bicicletas');*/
    Bicicleta.findById(req.params.id, function(err, bicicleta) {
        bicicleta.id = req.body.id;
        bicicleta.color = req.body.color;
        bicicleta.modelo = req.body.modelo;
        bicicleta.ubicacion = [req.body.lat, req.body.lng];
        bicicleta.save();

        res.redirect('/bicicletas');
    });
}

exports.bicicleta_delete_post = function(req, res) {
    /*Bicicleta.removeById(req.body.id);

    res.redirect('/bicicletas');*/
    /*Bicicleta.findById(req.params.id, function(err, bicicleta) {
        //bicicleta.id = req.body.id;
        bicicleta.removeById(req.body.id)
        res.redirect('/bicicletas');
    });*/

    Bicicleta.findByIdAndDelete(req.body.id, function(err) {
        if (err)
            next(err);
        else
            res.redirect('/bicicletas');
    });
}