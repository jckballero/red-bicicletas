var Bicicleta = require('../../models/bicicleta');

/*
exports.bicicleta_list = function(req, res) {
    Bicicleta.allBicis(function(err, bicis) {
        res.status(200).json({
            bicicletas: bicis
        });
    });
};

exports.bicicleta_create = function(req, res) {
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(function(err) {
        res.status(200).json({
            bicicleta: bici,
        });
    });
};

exports.bicicleta_delete = function(req, res) {
    Bicicleta.removeByCode(req.body.code, function(err, bici) {
        res.status(204).send();
    });
}
*/
exports.bicicleta_list = function(req, res) {
    Bicicleta.allBicis(function(err, bicis) {
        res.status(200).json({
            bicicletas: bicis
        });
    });
};

exports.bicicleta_create = function(req, res) {
    var ubicacion = [req.body.lat, req.body.lng];
    var bici = new Bicicleta({ code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: ubicacion });

    bici.save(function(err) {
        res.status(200).json(bici);
    });
}

exports.bicicleta_delete = function(req, res) {
    Bicicleta.removeByCode(req.body.code, function(err) {
        res.status(204).send();
    });
}